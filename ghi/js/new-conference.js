window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';
    const stateUrl = 'http://localhost:8000/api/states/';

    // Load available locations
    const response = await fetch(locationUrl);
    const data = await response.json();
    const locationSelect = document.getElementById('location');

    for (let location of data.locations) {
      const optionTag = document.createElement('option');
      optionTag.value = location.id
      optionTag.text = location.name;
      locationSelect.appendChild(optionTag);
    }
    const form = document.getElementById('create-conference-form');
    form.addEventListener('submit', async (event) => {
      event.preventDefault();
      const formData = new FormData(form);
      const json = JSON.stringify(Object.fromEntries(formData));

      const fetchConfig = {
        method: 'post',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch('http://localhost:8000/api/conferences/', fetchConfig);
      if (response.ok) {
        form.reset();
        const newConference = await response.json();
      }
    });
  });
