const payloadCookie = await cookieStore.get("jwt_access_payload");
if (payloadCookie) {
    console.log(payloadCookie)
    // try {
        const encodedPayload = JSON.parse(payloadCookie.value);
        const decodedPayload = atob(encodedPayload);
        const payload = JSON.parse(decodedPayload);
        console.log(payload);

        const ConferenceLink = document.getElementById("new-location");
        if (payload.user.perms.includes('events.add_conference')) {
            ConferenceLink.classList.remove('d-none');
        }
        const LocationLink = document.getElementById("new-conference");
        if (payload.user.perms.includes('events.add_location')) {
            LocationLink.classList.remove('d-none');
        }
    // }
    // catch (error) {
    //     console.error('Error parsing JSON:', error);
    // }
}
