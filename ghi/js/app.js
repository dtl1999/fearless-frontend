function createCard(name, description, pictureUrl, startDate, endDate, location) {
    const options = {month: 'short', day: 'numeric', year: "numeric"};
    const starts = new Date(startDate).toLocaleDateString('en-US', options);
    const ends = new Date(endDate).toLocaleDateString('en-US', options);
    const locationName = location;
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle mb-2 text-muted">${locationName}</p>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <small class="text-muted">${starts} - ${ends}</small>
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Handle HTTP error
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details);
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }
      }
    } catch (error) {
      console.error('Error fetching conference data: ', error.message)
    }
  });
