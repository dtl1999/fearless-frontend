window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      selectTag.classList.remove('d-none');

      const loadingIcon = document.getElementById('loading-conference-spinner');
      loadingIcon.classList.add('d-none');
    }

    const attendeeForm = document.getElementById('create-attendee-form');
    const successMessage = document.getElementById('success-message');

    attendeeForm.addEventListener('submit', async (event) => {
      event.preventDefault();
      const formData = new FormData(attendeeForm);
      const attendeeData = Object.fromEntries(formData.entries());

      const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(attendeeData),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      try {
        const response = await fetch('http://localhost:8001/api/attendees/', fetchConfig);
        if (response.ok) {
          attendeeForm.reset();
          successMessage.classList.remove('d-none');
          attendeeForm.classList.add('d-none');
        } else {
          throw new Error('Failed to create attendee.');
        }
      } catch (error) {
        console.error(error);
        // handle error
      }
    
    });
  });
