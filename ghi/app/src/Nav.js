function Nav() {
    return (
        <ul className="nav">
            <li className="nav-item">
                <a className="nav-link" href="http://localhost:3000/">Home</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="http://localhost:3000/new-location.html">New Location</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="http://localhost:3000/new-conference.html">New Conference</a>
            </li>
        </ul>

    );
  };

  export default Nav;
